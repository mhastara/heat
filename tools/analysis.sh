#!/bin/bash

lcov --directory . --capture --output-file heat.lcov
/builds/sonar/lcov-to-cobertura-xml-1.6/lcov_cobertura/lcov_cobertura.py heat.lcov --output heat-coverage.xml

export DEFINITIONS=""
export CPPCHECK_INCLUDES="-I."
export SOURCES_TO_EXCLUDE="-ibuild/CMakeFiles/"
export SOURCES_TO_ANALYZE="."

cppcheck -v -f --language=c --platform=unix64 --enable=all --xml --xml-version=2 --suppress=missingIncludeSystem ${DEFINITIONS} ${CPPCHECK_INCLUDES} ${SOURCES_TO_EXCLUDE} ${SOURCES_TO_ANALYZE} 2> heat-cppcheck.xml

rats -w 3 --xml ${SOURCES_TO_ANALYZE} > heat-rats.xml

bash -c 'find ${SOURCES_TO_ANALYZE} -regex ".*\.c\|.*\.h" | vera++ - -showrules -nodup |& /builds/sonar/sonar-cxx/sonar-cxx-plugin/src/tools/vera++Report2checkstyleReport.perl > heat-vera.xml'

mpirun "-np" "4" valgrind --xml=yes --xml-file=heat-valgrind.xml --memcheck:leak-check=full --show-reachable=yes --suppressions=/usr/share/openmpi/openmpi-valgrind.supp --suppressions=tools/heat-valgrind.supp "./build/heat_par" "10" "10" "200" "2" "2" "0"

/builds/sonar/sonar-scanner-2.9.0.670/bin/sonar-scanner -Dsonar.login=c1416c617418f890576a747223ac9f6cd86353d6

